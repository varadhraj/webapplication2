variables:
  NUGET_PATH: 'C:\tools\nuget.exe'
  MSBUILD_PATH: 'C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\MSBuild\Current\Bin\msbuild.exe'    

stages:
  - build
  - test

build_job:
  stage: build
  only:
    - branches
  script:
    - '& "$env:NUGET_PATH" restore'
    - '& "$env:MSBUILD_PATH" /p:Configuration=Release /clp:ErrorsOnly'
  artifacts:
    expire_in: 2 days

